/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : tours

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 27/06/2019 09:48:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for store_member
-- ----------------------------
DROP TABLE IF EXISTS `store_member`;
CREATE TABLE `store_member`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '店铺ID',
  `vip_no` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'vip编号',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '会员微信OPENID',
  `seller_openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商家版小程序openid',
  `wx_openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公众号openid',
  `unionid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'unionid唯一标识',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '会员手机号',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录密码',
  `pay_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '余额支付密码',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '会员昵称',
  `headimg` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '会员头像',
  `user_type` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '用户类型 1-普通用户 2-商家用户',
  `sex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '性别',
  `level` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '会员级别',
  `total_amount` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '消费累计额度 用于用户等级',
  `user_money` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '用户金额',
  `distribut_money` decimal(8, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '累积分佣金额',
  `frozen_money` double(8, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '冻结金额',
  `underling_number` int(5) UNSIGNED NULL DEFAULT 0 COMMENT '用户下线总数',
  `pay_points` int(8) UNSIGNED NULL DEFAULT 0 COMMENT '消费积分(可用积分)',
  `frozen_points` int(8) UNSIGNED NULL DEFAULT 0 COMMENT '冻结积分',
  `address_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '默认收货地址',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '会员个性签名',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '会员状态0待审核 1-已审核 2-已拒绝',
  `create_time` bigint(20) UNSIGNED NULL DEFAULT 0,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status_time` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '审核时间',
  `login_at` datetime(0) NULL DEFAULT NULL,
  `login_num` int(8) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `is_distribut` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否为分销商 0 否 1 是',
  `first_leader` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '第一个上级',
  `second_leader` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '第二个上级',
  `third_leader` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '第三个上级',
  `is_super` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否是超管 1-是 0不是',
  `spread_qrcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '分销推广二维码图片',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_store_member_phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商城会员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of store_member
-- ----------------------------
INSERT INTO `store_member` VALUES (7, 25, '', 'oqwKL5Zm8oINt29qiXdbtgYA9EDo', 'orRaL5Z5dEZpoiCasM8yaNhd2-c0', 'oC4411gveEovd6aj4eMpmu_jGU08', 'oC1EY5xdoEmfnep5TvQhDm2EtpCA', '15136175246', 'e10adc3949ba59abbe56e057f20f883e', '$2y$10$xgZaI3sM82Dhr/rmVhk8GeZVWD1xRmDDM3/DeSvyTisi9XnmDrUtW', '不知@道人', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83epd9Nb551ciaTWNr4jr6rfZhLQ1f9mkUaOzXVplRrVE64dib9A6JaGBxicLjR1m9XictwUfp03ibKW37dQ/132', 2, '1', 1, 0.00, 97018.38, 0.00, 0.00, 0, 42, 0, 0, '', 0, 1553250624, '2019-03-22 18:30:24', 0, '2019-05-10 15:45:14', 26, 0, 0, 0, 0, 1, '');
INSERT INTO `store_member` VALUES (8, 29, '', 'oqwKL5Wk0ov0izG3fRWuHysomPAA', 'orRaL5V3eAagxNpYJlpVrClIIj0A', 'oC4411lzUYKnn3LiW0pXDklghslo', 'oC1EY5_REYjW7RfVIG_G5RZhTIEY', '18905970273', 'e10adc3949ba59abbe56e057f20f883e', '$2y$10$ezW3ao7aVsLRepuamtWNcemsiA9ok1EPH9vm34CYaqbtR5WMpOXpm', '煎饼。', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erR0ic49TiamsHQGoHaeVm0ZuYBLjicTodtQJyfLBted5OJaVSkSZf6oFF02KF9gk1njYFeKyaiaW7VsQ/132', 2, '1', 1, 0.00, 6803.99, 0.00, 0.00, 0, 55, 0, 0, '', 0, 1553319329, '2019-03-23 13:35:29', 0, '2019-04-24 14:21:14', 12, 0, 0, 0, 0, 1, '');

SET FOREIGN_KEY_CHECKS = 1;
