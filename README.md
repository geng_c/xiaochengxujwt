1、本程序是基于Light Security light-security-spring-boot-starter，为什么用它？

1.1、因为简单，就跟作者所说一样。

2、技术栈：springboot+mybatis-plus+jwt+mapstruct+lombok+redis+swagger

3、导入resources/store_member.sql,修改yml数据库配置即可,注意建议与简单交互全部以json方式的形式，

    3.1、http://localhost:8009/auth/login 登陆授权

![输入图片说明](https://images.gitee.com/uploads/images/2019/0627/110842_45244c28_477893.png "微信截图_20190627110826.png")

    3.2、http://localhost:8009/user/info  获取用户信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0627/111118_91611de3_477893.png "_20190627110917.png")

4、test里面有代码自动生成功能 可以快速生成实体与dao



